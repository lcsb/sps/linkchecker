
# Linkchecker cron service

This periodically checks the links in a website installation via GitLab CI.

The URLs that are examined by this particular instance are listed in file
[`urls.txt`](urls.txt).

Remember that you should add a pipeline schedule that actually runs the
linkchecker on your forks of this repository.
